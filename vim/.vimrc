set backspace=indent,eol,start
set mouse=a
set clipboard=unnamed
set paste

set number
set ruler
set showcmd
set cursorline
set showmatch

set incsearch
set hlsearch

set nobackup
set nowb
set noswapfile

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set ai

syntax on
set background=dark
colorscheme monokai

au BufRead,BufNewfile *.yara set filetype=yara
au BufRead,BufNewfile *.yar set filetype=yara
